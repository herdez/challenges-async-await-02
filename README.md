# NodeJS Challenges (Instructions) - Solving Problems  

## Async and Await

### Objectives

- To use `async/await` pattern.
- To use `Error Handling`.

### Description

The company 'AsyncWt Co., Ltd.' has a problem with `multiple async functions in series`.

`Chief Technoloy officer` has given a `POC` 'Proof of Concept' to the software
development team with the following requirements:


- Each function has the responsability of giving a piece of data, when async 
functions are called in series, we got the following output:


Output example:

```
Javier Coller, email: coller@gmail.com, account: true

```

- Define the following functions that `resolve` this data after `3 seg.`:


1. `functionToGiveName()`     --> `fullName`.

2. `functionToGiveEmail()`    --> `email`.

3. `functionToGiveAccount()`  --> `account`.

4. `integrateData()`          --> `fullName`, `email`, `account`.


//+++ Constrainsts

- When an account doesn't exist then a new error is created: `Account is false`.

- Tests must be true.


## Deliverables


- When the following data is input...


```javascript
const fullName = 'Javier Coller',
      email = 'coller@gmail.com',
      account = true
```

Test 1...

```javascript
integrateData(fullName,  email, account)

.then(data => console.log("Test 1...", data.data === 'Javier Coller, email: coller@gmail.com, account: true'))
    
.catch(e => console.log(e))
```


Output is...

```
Test 1... true
```


- When the following data is input...


```javascript
const fullName = 'Roy Mirror',
      email = 'mirror@gmail.com',
      account = true
```

Test 2...

```javascript
integrateData(fullName,  email, account)

.then(data => console.log("Test 2...", data.data === 'Roy Mirror, email: mirror@gmail.com, account: true'))
    
.catch(e => console.log(e))
```


Output is...

```
Test 2... true
```


- When an account doesn't exist...

```javascript
const fullName = 'Michelle Goyer',
      email = 'goyer@gmail.com',
      account = false
```

Test 3...

```javascript
integrateData(fullName,  email, account)
   
.catch(e => console.log("Test 3...", e.message === 'Account is false'))
```

Output is ...

```
Test 3... true
```
