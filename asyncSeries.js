/*

The company 'AsyncWt Co., Ltd.' has a problem with multiple async functions in 
series.

Chief Technoloy officer has given a POC 'Proof of Concept' to the software
development team with the following requirements:


- Each function has the responsability of giving a piece of data, when async 
functions are called in series, we got the following output:


Output example:

```
Javier Coller, email: coller@gmail.com, account: true

```

- Define the following functions that 'resolve' this data after '3 seg.':

- functionToGiveName()      --> 'fullName'.

- functionToGiveEmail()     --> 'email'.

- functionToGiveAccount()   --> 'account'.

- integrateData()           --> 'fullName', 'email', 'account'.


//+++ Constrainsts

- When an account doesn't exist then a new error is created: 'Account is false'.

- Tests must be true.

*/


//+++ YOUR CODE GOES HERE



//functionToGiveName()





//functionToGiveEmail()





//functionToGiveAccount()





//integrateData()






// *~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*
// *~*~*~*~*~*~*~* Tests (Don't Touch) *~*~*~*~*~*~*~*~*
// *~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*


// Uncomment to test

/*

console.log("*~*~*~* Test 1: Javier Coller *~*~*~*")

const fullName = 'Javier Coller',
      email = 'coller@gmail.com',
      account = true


integrateData(fullName,  email, account)

.then(data => console.log("Test 1...", data.data === 'Javier Coller, email: coller@gmail.com, account: true'))
    
.catch(e => console.log(e))

*/

/*

console.log("*~*~*~* Test 2: Roy Mirror *~*~*~*")


const fullName = 'Roy Mirror',
      email = 'mirror@gmail.com',
      account = true



integrateData(fullName,  email, account)

.then(data => console.log("Test 2...", data.data === 'Roy Mirror, email: mirror@gmail.com, account: true'))
    
.catch(e => console.log(e))

*/

/*
console.log("*~*~*~* Test 3: Account is false *~*~*~*")

const fullName = 'Michelle Goyer',
      email = 'goyer@gmail.com',
      account = false



integrateData(fullName,  email, account)
   
.catch(e => console.log("Test 3...", e.message === 'Account is false'))

*/

